include:
  - project: va1erify-gitlab-ci/ansible-ci
    file: .ansible-playbook.yml
  - project: va1erify-gitlab-ci/gitlab-terraform-ci
    file: .terraform-gitlab.yml

stages:
  - validate
  - build
  - deploy-infrastructure
  - deploy-ansible
  - cleanup

variables:
  TF_ROOT: "${CI_PROJECT_DIR}/infrastructure"  # The relative path to the root directory of the Terraform project
  TF_STATE_NAME: default      # The name of the state file used by the GitLab Managed Terraform state backend

before_script:
  - cat infrastructure/terraformrc.config > ~/.terraformrc

fmt:
  extends: .terraform:fmt
  needs: [ ]
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web"'

validate:
  extends: .terraform:validate
  needs: [ ]
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web"'

build:
  extends: .terraform:build
  environment:
    name: $TF_STATE_NAME
    action: prepare
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: manual

deploy-infrastructure:
  extends: .terraform:deploy
  dependencies:
    - build
  environment:
    name: $TF_STATE_NAME
    action: start
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: manual
  after_script:
    - gitlab-terraform output -json
    - gitlab-terraform output -json > terraform_outputs.json
  artifacts:
    paths:
      - terraform_outputs.json

deploy-ansible:
  stage: deploy-ansible
  dependencies:
    - deploy-infrastructure
  variables:
    PATH_TO_PLAYBOOK: "playbook.yaml"
    LIMIT: "all"
    EXTRAVARS: "empty=true"
    TAGS: "all"
    PATH_TO_REQUIREMENTS: "requirements.yaml"
    PATH_TO_INVENTORY: "/builds/va1erify-deusops/task-26-01-2024/normal/ansible/inventories/dev/hosts"
    PATH_TO_ANSIBLE_CONFIG: "./ansible.cfg"
    DEBUG_LEVEL: "-vv"
    PATH_TO_TERRAFORM_OUTPUT: "/builds/va1erify-deusops/task-26-01-2024/normal/terraform_outputs.json"
    PATH_TO_PYTHON_SCRIPT: "/builds/va1erify-deusops/task-26-01-2024/normal/infrastructure/generate_hosts.py"
    PATH_TO_ANSIBLE_DIRECTORY: "/builds/va1erify-deusops/task-26-01-2024/normal/ansible"
    PATH_TO_HOSTS_TEMPLATE: "/builds/va1erify-deusops/task-26-01-2024/normal/infrastructure/hosts_template.j2"
  before_script:
    - python3 $PATH_TO_PYTHON_SCRIPT $PATH_TO_TERRAFORM_OUTPUT $PATH_TO_HOSTS_TEMPLATE
    - mv hosts $PATH_TO_INVENTORY
    - cat $PATH_TO_INVENTORY
    - cd $PATH_TO_ANSIBLE_DIRECTORY
    - eval $(ssh-agent -s)
    - chmod 400 "$SSH_PRIVATE_KEY"
    - base64 -d "$SSH_PRIVATE_KEY" | ssh-add -
    - chmod 0600 -R $PATH_TO_ANSIBLE_DIRECTORY
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ansible-galaxy collection install community.docker
    - ansible-galaxy collection install ansible.posix
  extends: .run-playbook
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: manual

cleanup:
  extends: .terraform:destroy
  dependencies:
    - cleanup
  needs: [ ]
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: manual