# Решение уровня Normal [Проекта](https://gitlab.com/va1erify-deusops/task-26-01-2024/about_project)

## Структура репозитория

```
.
├── ansible                                 # провижн
│ ├── ansible.cfg
│ ├── inventories
│ │ └── dev
│ │     ├── group_vars
│ │     │ └── all
│ │     │     └── main.yaml
│ │     ├── hosts
│ │     └── templates
│ │         ├── docker-compose.service.j2   # template для сервиса
│ │         ├── docker-compose.yaml.j2      # template для docker-compose
│ │         └── haproxy.cfg.j2              # template для haproxy
│ ├── playbook.yaml
│ └── requirements.yaml                     # 3 роли (docker, haproxy, systemd)
├── .gitlab-ci.yml                          # CI/CD
├── infrastructure                          # инфраструктура
│ ├── backend.tf
│ ├── cloud-config
│ ├── generate_hosts.py                     # скрипт для формирования inventory
│ ├── hosts_template.j2                     # template для inventory
│ ├── main.tf
│ ├── outputs.tf
│ ├── provider.tf
│ ├── terraformrc.config                    # для загрузки провайдера с зеркала
│ └── variables.tf
└── README.md
```

## Описание CI/CD процесса

Этот проект включает в себя CI/CD процесс, который автоматизирует развертывание инфраструктуры и приложений с
использованием Terraform и Ansible. CI/CD пайплайн разделен на следующие этапы:

1. **validate&fmt:** Проверяет корректность Terraform конфигурации.
2. **build:** Билдит инфраструктуру в соответствии с Terraform конфигурацией.
3. **deploy-infrastructure:** Развертывает инфраструктуру, подготовленную Terraform, и сохраняет вывод Terraform в
   формате JSON.
4. **deploy-ansible:** С использованием Ansible осуществляется провижн.
5. **cleanup:** Уничтожает созданные Terraform ресурсы.

## Gitlab CI/CD vars

| Type     | Key             | Description                                                                                                  |
|----------|-----------------|--------------------------------------------------------------------------------------------------------------|
| Variable | CLOUD_ID        | Yandex cloud ID. Used as a variable in the Terraform pipeline to identify the Yandex Cloud ID.               |
| Variable | FOLDER_ID       | Yandex folder ID. Used as a variable in the Terraform pipeline to specify the target folder in Yandex Cloud. |
| Variable | TOKEN           | Masked Yandex token. Used as a variable in the Terraform pipeline to authenticate with the Yandex Cloud API. |
| FILE     | SSH_PRIVATE_KEY | Masked Key for accessing the VM.                                                                             

## Pipeline CI/CD vars

| Key                       | Description                                                                   |
|---------------------------|-------------------------------------------------------------------------------|
| TF_ROOT                   | The relative path to the root directory of the Terraform project              |
| TF_STATE_NAME             | The name of the state file used by the GitLab Managed Terraform state backend | 
| PATH_TO_PLAYBOOK          | Path to ansible playbook                                                      |
| LIMIT                     | Limits for ansible playbook                                                   |
| EXTRAVARS                 | Extra variables for ansible playbook                                          |
| TAGS                      | Tags for ansible playbook                                                     |
| PATH_TO_REQUIREMENTS      | Path to file with ansible requirements                                        |
| PATH_TO_INVENTORY         | Path to inventory file                                                        |
| PATH_TO_ANSIBLE_CONFIG    | Path to ansible.cfg                                                           |
| DEBUG_LEVEL               | Debug for ansible (-v,-vv,-vvv...)                                            |
| PATH_TO_TERRAFORM_OUTPUT  | Path to file with terraform outputs                                           |
| PATH_TO_ANSIBLE_DIRECTORY | Path to root directory of ansible                                             |
| PATH_TO_PYTHON_SCRIPT     | Path ro .py script for generating inventory file                              |
| PATH_TO_HOSTS_TEMPLATE    | Path to .j2 template for create inventory file                                

### Example Usage

- `TF_ROOT`: `"${CI_PROJECT_DIR}/infrastructure"`
- `TF_STATE_NAME`: `"default"`
- `PATH_TO_PLAYBOOK`: `"playbook.yaml"`
- `LIMIT`: `"all"`
- `EXTRAVARS`: `"empty=true"`
- `TAGS`: `"all"`
- `PATH_TO_REQUIREMENTS`: `"requirements.yaml"`
- `PATH_TO_INVENTORY`: `"/builds/va1erify-deusops/task-26-01-2024/normal/ansible/inventories/dev/hosts"`
- `PATH_TO_ANSIBLE_CONFIG`: `"./ansible.cfg"`
- `DEBUG_LEVEL`: `"-vv"`
- `PATH_TO_TERRAFORM_OUTPUT`: `"/builds/va1erify-deusops/task-26-01-2024/normal/terraform_outputs.json"`
- `PATH_TO_ANSIBLE_DIRECTORY`: `"/builds/va1erify-deusops/task-26-01-2024/normal/ansible"`
- `PATH_TO_PYTHON_SCRIPT`: `"/builds/va1erify-deusops/task-26-01-2024/normal/infrastructure/generate_hosts.py"`
- `PATH_TO_HOSTS_TEMPLATE`: `"/builds/va1erify-deusops/task-26-01-2024/normal/infrastructure/hosts_template.j2"`

## Используемые репозитории

- [Игра (приложение)](https://gitlab.com/va1erify-deusops/task-26-01-2024/2048-game)
- [Terraform модули для YC](https://gitlab.com/va1erify-terraform/yandex-modules/modules)
- [CI template для Ansible](https://gitlab.com/va1erify-gitlab-ci/ansible-ci)
- [CI template для использования Gitlab в качестве backend для Terraform](https://gitlab.com/va1erify-gitlab-ci/gitlab-terraform-ci)
- [Ansible роль для установки и запуска docker&docker-compose](https://gitlab.com/va1erify-ansible-roles/ansible-role-docker)
- [Ansible роль для установки и настройки hapoxy](https://gitlab.com/va1erify-ansible-roles/ansible-role-haproxy)
- [Ansible роль для создания systemd.service из шаблона, его включения и запуска](https://gitlab.com/va1erify-ansible-roles/ansible-role-systemd)



