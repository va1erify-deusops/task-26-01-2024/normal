import argparse
import json
from jinja2 import Template

# Создание парсера аргументов командной строки
parser = argparse.ArgumentParser(description='Generate hosts file from Terraform outputs')
parser.add_argument('terraform_outputs_path', type=str, help='Path to terraform_outputs.json')
parser.add_argument('template_path', type=str, help='Path to Jinja2 template file')

# Парсинг аргументов
args = parser.parse_args()

# Загрузка данных из JSON-файла
with open(args.terraform_outputs_path, 'r') as terraform_output:
    data = json.load(terraform_output)

# Извлечение IP-адресов из JSON
game_ips = [data['vm_game01_external_ip']['value'],
            data['vm_game02_external_ip']['value'],
            data['vm_game03_external_ip']['value']]
haproxy_ip = data['vm_haproxy_external_ip']['value']

# Загрузка шаблона Jinja2
with open(args.template_path, 'r') as template_file:
    template_content = template_file.read()

# Компиляция шаблона и вставка данных
template = Template(template_content)
rendered_template = template.render(game_ips=game_ips, haproxy_ip=haproxy_ip)

# Запись результата в файл hosts
with open('hosts', 'w') as hosts_file:
    hosts_file.write(rendered_template)

print('Hosts file generated successfully.')