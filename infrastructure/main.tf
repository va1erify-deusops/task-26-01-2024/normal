module "network" {
  source       = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-vpc-network/yandex-cloud/0.1.0"
  network_name = var.network_name
}

module "subnetwork" {
  source          = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/vps-subnet/yandex-cloud/0.1.0"
  cidr_v4         = var.subnetwork_cidr_v4
  depends_on      = [module.network]
  zone            = var.zone
  network_id      = module.network.network_id
  subnetwork_name = var.subnetwork_name
}

module "vm-haproxy" {
  source               = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-compute-instance/yandex-cloud/0.2.0"
  depends_on           = [module.subnetwork]
  subnetwork_id        = module.subnetwork.subnetwork_id
  name                 = "haproxy"
  internal_ip_address  = "10.10.10.10"
  hostname             = "haproxy"
  platform             = var.vm_platform
  ram                  = var.vm_ram
  cpu                  = var.vm_cpu
  core_fraction        = var.vm_core_fraction
  boot_disk_image_id   = var.vm_boot_disk_image_id
  boot_disk_size       = var.vm_boot_disk_size
  boot_disk_type       = var.vm_boot_disk_type
  boot_disk_block_size = var.vm_boot_disk_block_size
  nat                  = var.vm_nat
  preemptible          = var.vm_preemptible
  path_to_cloud_config = var.vm_path_to_cloud_config
}

module "vm-game01" {
  source               = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-compute-instance/yandex-cloud/0.2.0"
  depends_on           = [module.subnetwork]
  subnetwork_id        = module.subnetwork.subnetwork_id
  name                 = "game01"
  internal_ip_address  = "10.10.10.11"
  hostname             = "game01"
  platform             = var.vm_platform
  ram                  = var.vm_ram
  cpu                  = var.vm_cpu
  core_fraction        = var.vm_core_fraction
  boot_disk_image_id   = var.vm_boot_disk_image_id
  boot_disk_size       = var.vm_boot_disk_size
  boot_disk_type       = var.vm_boot_disk_type
  boot_disk_block_size = var.vm_boot_disk_block_size
  nat                  = var.vm_nat
  preemptible          = var.vm_preemptible
  path_to_cloud_config = var.vm_path_to_cloud_config
}

module "vm-game02" {
  source               = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-compute-instance/yandex-cloud/0.2.0"
  depends_on           = [module.subnetwork]
  subnetwork_id        = module.subnetwork.subnetwork_id
  name                 = "game02"
  internal_ip_address  = "10.10.10.12"
  hostname             = "game02"
  platform             = var.vm_platform
  ram                  = var.vm_ram
  cpu                  = var.vm_cpu
  core_fraction        = var.vm_core_fraction
  boot_disk_image_id   = var.vm_boot_disk_image_id
  boot_disk_size       = var.vm_boot_disk_size
  boot_disk_type       = var.vm_boot_disk_type
  boot_disk_block_size = var.vm_boot_disk_block_size
  nat                  = var.vm_nat
  preemptible          = var.vm_preemptible
  path_to_cloud_config = var.vm_path_to_cloud_config
}

module "vm-game03" {
  source               = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-compute-instance/yandex-cloud/0.2.0"
  depends_on           = [module.subnetwork]
  subnetwork_id        = module.subnetwork.subnetwork_id
  name                 = "game03"
  internal_ip_address  = "10.10.10.13"
  hostname             = "game03"
  platform             = var.vm_platform
  ram                  = var.vm_ram
  cpu                  = var.vm_cpu
  core_fraction        = var.vm_core_fraction
  boot_disk_image_id   = var.vm_boot_disk_image_id
  boot_disk_size       = var.vm_boot_disk_size
  boot_disk_type       = var.vm_boot_disk_type
  boot_disk_block_size = var.vm_boot_disk_block_size
  nat                  = var.vm_nat
  preemptible          = var.vm_preemptible
  path_to_cloud_config = var.vm_path_to_cloud_config
}
