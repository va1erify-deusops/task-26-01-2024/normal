# Creds
variable "token" {}
variable "cloud_id" {}
variable "folder_id" {}

# General
variable "zone" {
  default = "ru-central1-a"
}

# Network
variable "network_name" {
  default = "default-network"
}

# Subnet
variable "subnetwork_cidr_v4" {
  default = "10.10.10.0/24"
}

variable "subnetwork_name" {
  default = "default-subnetwork"
}

variable "vm_platform" {
  default     = "standard-v2"
  description = "Platform YCloud"
}

variable "vm_ram" {
  default     = 0.5
  description = "Count RAM in GB"
}

variable "vm_cpu" {
  default     = 2
  description = "Count CPU"
}

variable "vm_core_fraction" {
  default = 5
}

variable "vm_boot_disk_image_id" {
  default = "fd8t849k1aoosejtcicj"
}
variable "vm_boot_disk_size" {
  default = 5
}

variable "vm_preemptible" {
  default = true
}

variable "vm_boot_disk_type" {
  default = "network-hdd"
}

variable "vm_boot_disk_block_size" {
  default = 4096
}

variable "vm_nat" {
  default = true
}

variable "vm_path_to_cloud_config" {
  default = "./cloud-config"
}